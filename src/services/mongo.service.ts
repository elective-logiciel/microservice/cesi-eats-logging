import mongoose, { connect, Schema, model, ObjectId } from 'mongoose';

const config = require('../config')

const mongoDb = connect(config.mongo.server + config.mongo.database);

export const MongoOptions = {
    dbinfo: {
        level: "info",
        collection: "Logging",
        db: config.mongo.server,
        dbName: config.mongo.database,
        includeIds: true,
        decolorize: true,
        metaKey: "service",
        options: { useNewUrlParser: true, useUnifiedTopology: true },
        maxsize: 52428800, // 50MB
    }
}

export const Logs = mongoose.connection.collection('Logging');
