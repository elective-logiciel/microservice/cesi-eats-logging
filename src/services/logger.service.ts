import winston from 'winston'
import("winston-mongodb")
require('winston-mongodb');
import { MongoOptions } from './mongo.service'

const config = require('../config')
const { transports } = winston
const { combine, timestamp, json } = winston.format;

timestamp({
    format: 'YYYY-MM-DD hh:mm:ss.SSS A', // 2022-01-25 03:23:10.350 PM
})

export const logger = winston.createLogger({
    level: config.log_level || 'info',
    format: combine(timestamp(), json(), winston.format.cli()),
    defaultMeta: {
        service: 'default'
    },
    transports: [
        new winston.transports.Console(),
        new winston.transports.MongoDB(MongoOptions.dbinfo)
    ]
})
