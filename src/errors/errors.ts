import { BaseError } from "./baseError"

const httpStatusCodes = require('./httpStatusCodes')

class Error400 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.BAD_REQUEST,
        name = 'Bad Request'
    ) {
        super(message, statusCode, name)
    }
}

class Error401 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.UNAUTHORIZED,
        name = 'Unauthorized'
    ) {
        super(message, statusCode, name)
    }
}

class Error403 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.FORBIDDEN,
        name = 'Forbidden'
    ) {
        super(message, statusCode, name)
    }
}

class Error404 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.NOT_FOUND,
        name = 'Not Found'
    ) {
        super(message, statusCode, name)
    }
}

class Error408 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.TIME_OUT,
        name = 'Request Time-out'
    ) {
        super(message, statusCode, name)
    }
}

class Error412 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.PRECONDITION_FAILED,
        name = 'Precondition Failed'
    ) {
        super(message, statusCode, name)
    }
}

class Error498 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.TOKEN_INVALID,
        name = 'Token expired/invalid'
    ) {
        super(message, statusCode, name)
    }
}

class Error500 extends BaseError {
    constructor(
        message,
        statusCode = httpStatusCodes.INTERNAL_SERVER,
        name = 'Internal Server'
    ) {
        super(message, statusCode, name)
    }
}

export { Error400, Error401, Error403, Error404, Error408, Error498, Error412, Error500 }