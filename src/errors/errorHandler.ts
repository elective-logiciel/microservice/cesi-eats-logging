function logError (err, req, res, next) {
    console.error(err)

    next(err)
}

function returnError (err, req, res, next) {
    res.status(err.statusCode || 500).json({
        name: err.name,
        status: err.statusCode || 500,
        message: err.message
    })
}

export { logError, returnError }