function returnSuccess(res, data) {
    res.status(200).json({
        name: 'OK',
        status: 200,
        data: data
    })
}

function returnCreated(res) {
    res.status(201).json({
        name: 'Created',
        status: 201,
        message: "Ressource succesfully created."
    })
}

function returnUpdated(res) {
    res.status(204).json({
        name: 'No Content',
        status: 204,
        message: "Ressource succesfully updated."
    })
}

function returnDeleted(res) {
    res.status(204).json({
        name: 'No Content',
        status: 204,
        message: "Ressource succesfully deleted."
    })
}

export { returnSuccess, returnCreated, returnUpdated, returnDeleted }