const config = require('./config')
const app = require('./server')

/**
 * Lauching serveur
 */
app.listen(config.port, () => console.log('Starting server listening on port', config.port))