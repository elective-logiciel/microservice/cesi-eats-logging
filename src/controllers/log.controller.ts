import { Router } from 'express';
import { Error412, Error498 } from '../errors/errors';
import '../services/logger.service'
import { Log } from '../models/log.model'
import { returnSuccess, returnCreated } from '../errors/success';
import { logService } from '../datas/logging.data';

const logRouter = Router();
const dataLog = new logService();

logRouter.post('/info', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const log = new Log ({
            message: req.body.message,
            status: req.body.status,
            status_name: req.body.status_name,
            service_name: req.body.service_name
        })

        log.IsMessageValid()
        log.IsServiceNameValid()
        log.IsStatusValid()
        log.IsStatusNameValid()

        dataLog.addInfo(log)

        returnCreated(res)

    } catch (err) {
        next(err)
    }
});

logRouter.post('/warn', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const log = new Log({
            message: req.body.message,
            status: req.body.status,
            status_name: req.body.status_name,
            service_name: req.body.service_name
        })

        log.IsMessageValid()
        log.IsServiceNameValid()
        log.IsStatusValid()
        log.IsStatusNameValid()

        dataLog.addWarn(log)

        returnCreated(res)

    } catch (err) {
        next(err)
    }
});

logRouter.post('/error', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const log = new Log({
            message: req.body.message,
            status: req.body.status,
            status_name: req.body.status_name,
            service_name: req.body.service_name
        })

        log.IsMessageValid()
        log.IsServiceNameValid()
        log.IsStatusValid()
        log.IsStatusNameValid()

        dataLog.addError(log)

        returnCreated(res)

    } catch (err) {
        next(err)
    }
});

logRouter.get('/level/all', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const all = await dataLog.getAll()

        returnSuccess(res, all)

    } catch (err) {
        next(err)
    }
});

logRouter.get('/:service_name', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const log = new Log({
            service_name: req.params.service_name
        })

        log.IsServiceNameValid()

        const logService = await dataLog.getByServiceName(log)

        returnSuccess(res, logService)

    } catch (err) {
        next(err)
    }
});

logRouter.get('/level/info', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const infos = await dataLog.getInfo()

        returnSuccess(res, infos)

    } catch (err) {
        next(err)
    }
});

logRouter.get('/level/warn', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const warns = await dataLog.getWarn()

        returnSuccess(res, warns)

    } catch (err) {
        next(err)
    }
});

logRouter.get('/level/error', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const errors = await dataLog.getError()

        returnSuccess(res, errors)

    } catch (err) {
        next(err)
    }
});

export {logRouter}