import {logger} from '../services/logger.service'
import { Log } from '../models/log.model'
import winston from 'winston'
import {  Logs } from '../services/mongo.service'

const config = require('../config')


export class logService {

    public addInfo(log: Log) {

        var message = log.status + ' ' + log.status_name + ' : ' + log.message

        logger.info(message, { service: log.service_name })
    }

    public addWarn(log: Log) {

        var message = log.status + ' ' + log.status_name + ' : ' + log.message

        logger.warn(message, { service: log.service_name })
    }

    public addError(log: Log) {

        var message = log.status + ' ' + log.status_name + ' : ' + log.message

        logger.error(message, { service: log.service_name })
    }

    public async getAll() {
            
        const all = await Logs.find().sort({ timestamp: -1 }).toArray()
        
        return all

    }

    public async getByServiceName(log: Log) {

        const all = await Logs.find({ meta: log.service_name }).sort({ timestamp: -1 }).toArray()

        return all

    }

    public async getInfo() {

        const infos = await Logs.find({ level: "info" }).sort({ timestamp: -1 }).toArray()

        return infos
    }

    public async getWarn() {

        const warns = await Logs.find({ level: "warn" }).sort({ timestamp: -1 }).toArray()

        return warns
    }

    public async getError() {

        const errors = await Logs.find({ level: "error" }).sort({ timestamp: -1 }).toArray()

        return errors
    }
}