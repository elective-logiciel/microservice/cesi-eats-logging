const assert = require("assert");
const dotenv = require("dotenv");

// read in the .env file
dotenv.config();

// capture the environment variables the application needs
const {
    PORT,
    HOST,
    MONGO_SERVER,
    MONGO_DATABASE,
    LOG_LEVEL,
} = process.env;


// validate the required configuration information
assert(PORT, "PORT configuration is required.");
assert(HOST, "HOST configuration is required.");
assert(MONGO_SERVER, "MONGO_SERVER configuration is required.");
assert(MONGO_DATABASE, "MONGO_DATABASE configuration is required.");
assert(LOG_LEVEL, "LOG_LEVEL configuration is required.");

// export the configuration information
module.exports = {
    port: PORT,
    host: HOST,
    mongo: {
        server: MONGO_SERVER,
        database: MONGO_DATABASE,
    },
    log_level: LOG_LEVEL,
};