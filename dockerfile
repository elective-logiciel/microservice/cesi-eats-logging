# build stage
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


# production stage
FROM node:lts-alpine as production-stage
WORKDIR /app
COPY package*.json /app
RUN npm install
COPY --from=build-stage /app/dist/src /app
COPY .env /app
EXPOSE 3000
CMD ["node", "/app/index.js"]
